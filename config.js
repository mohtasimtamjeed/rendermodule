//tailor to specific file you are calling
//define('three', ['./three.js'], function ( THREE ) {
  //window.THREE = THREE;
  //return THREE;
  //console.log("define ran");
//});

//config file for require

requirejs.config({
    baseUrl: "",
    paths: {
        THREE: "three.min",
        GLTFLoader: "GLTFLoader",
        OrbitControls: "OrbitControls",
        DomEvents: "threex.domevents"
    },
    shim: {

       "DomEvents": ["THREE"],
       "GLTFLoader": ["THREE"],
       "OrbitControls": ["THREE"]
       
    }
})
define('THREE', ['three.min.js'], function ( THREE ) {
  window.THREE = THREE;
  return THREE;
});
requirejs(["./draw.js"])