'use strict'

/*
structure of data received
app1.msg.Recvd.payload.floor = {
  id: 1,
  file: "01_1OG_Text_Mesh.gltf",
  //"02_2OG_Text_Mesh.gltf" for floor2
  //"03_3OG_Text_Mesh.gltf" for floor3
  //"04_4OG_Text_Mesh.gltf" for floor4
  //"05_5OG_Text_Mesh.gltf" for floor5
  //"06_6OG_Text_Mesh.gltf" for floor6
  selected: true,
  rooms: {
      0: {"floorid": 1,
          "id": 111,
          "roomNumber": 111,
          "available": true
      },
      1: {"floorid": 1,
          "id": 122,
          "roomNumber": 122,
          "available": true
      },
      3: {"floorid": 1,
          "id": 130,
          "roomNumber": 130,
          "available": true
      }
      
  }
}*/

const floor = {
  id: 1,
  file: "01_1OG_Text_Mesh.gltf",
  //"02_2OG_Text_Mesh.gltf" for floor2
  //"03_3OG_Text_Mesh.gltf" for floor3
  //"04_4OG_Text_Mesh.gltf" for floor4
  //"05_5OG_Text_Mesh.gltf" for floor5
  //"06_6OG_Text_Mesh.gltf" for floor6
  selected: true,
  rooms: {
      0: {"floorid": 1,
          "id": 111,
          "roomNumber": 111,
          "available": true
      },
      1: {"floorid": 1,
          "id": 122,
          "roomNumber": 122,
          "available": true
      },
      3: {"floorid": 1,
          "id": 130,
          "roomNumber": 130,
          "available": true
      }
      
  }
}




/*const roomData = app1.msgRecvd.payload.floor.rooms;
const floorID = app1.msgRecvd.payload.floor.id;
const floorFile = app1.msgRecvd.payload.floor.file;
const floorData = app1.msgRecvd.payload.floor;
*/


const roomData = floor.rooms;
const floorID = floor.id;
const floorFile = floor.file;
const floorData = floor;


//maybe we want to separate this logic from the app1 file
//uibuilder on change data
//pass in the data to render function?
//console.log(roomData);


/*uibuilder.onChange('msg', function(newVal){
    console.log(newVal);
    //we could probably pull in floor roomdata from here and render after each injection event
});*/

//define(["THREE", "DomEvents", "OrbitControls", "GLTFLoader"], function(THREE){ DEFINE keyword was for using it with requirejs
   // console.log(THREE);

var selectedRoomID;
    
(function(){
  init();


function init() {
  const canvas = document.querySelector('#c')
  var oControls;
  var light, spotlight, loader;

  const w = canvas.clientWidth;
  const h = canvas.clientHeight;
  const viewSize = h;
  const aspectRatio = w / h;

  const viewport = {
      viewSize: viewSize,
      aspectRatio: aspectRatio,
      left: (-aspectRatio * viewSize) / 2,
      right: (aspectRatio * viewSize) / 2,
      top: viewSize / 2,
      bottom: -viewSize / 2,
      near: -500,
      far: 1000
  }

  const scene = new THREE.Scene();
  //const camera = new THREE.OrthographicCamera( window.innerWidth / - 50, window.innerWidth / 50, window.innerHeight / 50, window.innerHeight / -50, - 500, 1000);
  //rewrite this to take values from canvas 
  const camera = new THREE.OrthographicCamera ( 
    viewport.left, 
    viewport.right, 
    viewport.top, 
    viewport.bottom, 
    viewport.near, 
    viewport.far 
  );
  //console.log(window.innerWidth / - 50, window.innerWidth / 50, window.innerHeight / 50, window.innerHeight / -50, - 500, 1000)

  const renderer = new THREE.WebGLRenderer({canvas,  antialias: true, alpha: true});
  //console.log(renderer.domElement);
  renderer.setClearColor(0xEEEEEE, 1);
  //renderer.shadowMap.enabled = true;
  //renderer.setSize(canvas.innerWidth, canvas.innerHeight);
  renderer.setSize(window.innerWidth, window.innerHeight);
  //document.body.appendChild(renderer.domElement);


  //THREEx domevents plugin
  const domEvents = new THREEx.DomEvents(camera, renderer.domeElement);

//  var boxGeometry = new THREE.IcosahedronGeometry(0, 1);
  //var boxMaterial = new THREE.MeshPhongMaterial({
    //color: 0x99FFFF,
    //shading: THREE.FlatShading
  //});
  //var cube = new THREE.Mesh(boxGeometry, boxMaterial);
  //cube.castShadow = true;
  //cube.position.x = 0;
  //cube.position.y = 0;
  //cube.position.z = 0;
  //cube.position works so orbit control can be centered on this origin
  //scene.add(cube);

  //mouse = new THREE.Vector2();

  /* function getMousePosition(event) {
    var rect = renderer.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX - rect.left ) / ( rect.width - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;
  } */
    
    
    

    //mouse.x = (event.clientX/renderer.domElement.clientWidth) * 2 - 1;
    //mouse.y = (event.clientY/renderer.domElement.clientWidth) * -2 + 1;
    //console.log(mouse.x, mouse.y);

  
  //var axesHelper = new THREE.AxesHelper(3);
  //scene.add(axesHelper);

  var ambiColor = "#ffffff";
  var ambientLight = new THREE.AmbientLight(ambiColor, 1.1);
  scene.add(ambientLight);


  const gltfloader = new THREE.GLTFLoader();
  
  
  var disabled = ["Elevator", "Hallway", "Service", "Fire", "Escape", "Technik"];

  var floorplan, selectedUUID, boxHelper, boxCenter;
  const unavailableTexture = new THREE.MeshLambertMaterial({color: 0xF3FAFC, shading: THREE.SmoothShading,  flatShading: true, polygonOffset: true,
    polygonOffsetFactor: 1, polygonOffsetUnits: 1})
  const availableTexture = new THREE.MeshLambertMaterial({ color: 0x4CADA9, shading: THREE.SmoothShading, flatShading: true, polygonOffset: true,
    polygonOffsetFactor: 1, polygonOffsetUnits: 1});
  const symbolTexture = new THREE.MeshLambertMaterial({color: 0x000000, shading: THREE.SmoothShading,  flatShading: true, polygonOffset: true,
      polygonOffsetFactor: 1, polygonOffsetUnits: 1})
    

  const clickedTexture =  new THREE.MeshLambertMaterial({ color: 0xF09537, shading: THREE.SmoothShading, flatShading: true, polygonOffset: true,
    polygonOffsetFactor: 1, polygonOffsetUnits: 1 });
 // clickedTexture.flatShading = true;
  //let texture = new THREE.TextureLoader().load("/resources/model/_150010BX.jpg");
  
  //async call to load gltf file
  
  
  
  gltfloader.load(floorFile, function(gltf) {
    floorplan = gltf.scene;
    //console.log("log",gltf.scene.children[ 0 ]);
    
    floorplan.traverse(function(node) {
        
      if(node.isMesh) {//instanceof THREE.Mesh or .isMesh   (&& !node.disabled) used to be inside if logic
        node.disabled = true;
        
        
        for (var key in roomData) {
            //console.log(roomData[key])if(roomData[key].name == node.userData.name) {}
           // console.log(typeof(node.userData.name));  //YOU HAVE TO CHANGE THIS LOGIC LATER DEPENDING ON DATA STRUCTURE FROM INJECTED DATA
            console.log(Number(roomData[key].roomNumber),Number(String(node.userData.name)));
            if(Number(roomData[key].roomNumber) == Number(node.userData.name)) {
                console.log("room found");
                node.disabled = !roomData[key].available//? false : true; 
            }
        }

        
        
        let names = node.userData.name.split(" ");
        console.log(node.userData,names)
        
        
        names.forEach(function(name) {
          for(var i = 0; i < disabled.length; i++) {
            if (name === disabled[i]) {
              node.disabled = true;
              node.material = unavailableTexture;
            }
          }
        })
        
        names = node.userData.name.split(".");//logic for material for  symbol
        if (node.isMesh && names[0] == "Circle") {
          node.disabled = true;
          node.material = symbolTexture;
        }
        else if (node.isMesh && names[0] == "Text") {
          node.disabled = true;
          node.material = symbolTexture;
        }

        


        node.material.side = THREE.DoubleSide;
        
        //check node userdata and get availability through async call
        //put domEvents addeventlistener to only the nodes that show availability
        //add domEvents for touch interfaces
        node.material.polygonOffset = true;
        node.material.polygonOffsetFactor = 1;
        node.material.polygonOffsetUnits = 1;
        
        
        if(!node.disabled) {//if not disabled logic begin


          node.material = availableTexture;

          domEvents.addEventListener(node, "touchstart", function() {
          
            console.log(node.uuid, node.userData);
            
            if (selectedUUID !== node.uuid) {//you can actually check if this node is selected by checking selectedUUID !== node.uuid
              //console.log(node.material);
              //set previous nodes materials to availableTexture
              floorplan.traverse(function(node) {
                if (node.uuid == selectedUUID) {
                  node.material = availableTexture;
                }
              })            
              node.selected = true;
              selectedUUID = node.uuid;
              //console.log(selectedUUID);
              //console.log(node.selected);
              node.material = clickedTexture;
              let userData = JSON.stringify(node.userData);
              //console.log(typeof(String(userData)), String(userData));
            }
            else {
  
              //if current node was selected and deselecting then make the selectedUUID null
              
              node.selected = false;
              //console.log(node.uuid);//consider storing uuid in a variable with a scope above loader call, to check for double selection
              node.material = availableTexture;
              selectedUUID = null;
              //console.log(selectedUUID);
  
            }
            domEvents.addEventListener(node, "touchend", function() {
              //console.log(node.uuid, node.userData);
                for (var key in roomData) {//can be functionalized and just call with node.userData and roomData I guess, because the same is repeated for click function emit() {}
                    //console.log(roomData[key])if(roomData[key].name == node.userData.name) {}
                   // console.log(typeof(node.userData.name));  //YOU HAVE TO CHANGE THIS LOGIC LATER DEPENDING ON DATA STRUCTURE FROM INJECTED DATA
                    //console.log(typeof(roomData[key].roomNumber));
                    if(Number(roomData[key].roomNumber) == Number(node.userData.name)) {
                        console.log("touched");
                        selectedRoomID = roomData[key].id;
                    }
                }     
                uibuilder.send({//this is where the msg is being sent
                //when you click this sends the room number
                    'topic': "data from click on room",
                    'payload': {
                        'message': selectedRoomID
                    }
                })
              
              
            })
          })
  
          domEvents.addEventListener(node, "mouseover", function() {
            console.log(node.uuid, node.userData);
            node.material = clickedTexture;
          })
          domEvents.addEventListener(node, "mouseout", function() {
            console.log("mouseout");
            if(selectedUUID !== node.uuid) {
              node.material = availableTexture;
            }
            
          })
          domEvents.addEventListener(node, "click", function() {
  
            //if other node is already selected
            //deselect that node and select current node
            if (selectedUUID !== node.uuid) {//you can actually check if this node is selected by checking selectedUUID !== node.uuid
              //console.log(node.material);
              
              //set previous nodes materials to availableTexture
                floorplan.traverse(function(node) {
                    if (node.uuid == selectedUUID) {
                        node.material = availableTexture;
                    }
                })            
              
                
              
              //compare node.userData.name and roomData.roomNumber, if same get the selected room ID
              
              selectedUUID = node.uuid;
              //console.log(node.disabled,selectedUUID);
                for (var key in roomData) {
                    //console.log(roomData[key])if(roomData[key].name == node.userData.name) {}
                   // console.log(typeof(node.userData.name));  //YOU HAVE TO CHANGE THIS LOGIC LATER DEPENDING ON DATA STRUCTURE FROM INJECTED DATA
                    //console.log(typeof(roomData[key].roomNumber));
                    if(Number(roomData[key].roomNumber) == Number(node.userData.name)) {
                        console.log("clicked");
                        selectedRoomID = roomData[key].id;
                    }
                }     
                
                /*uibuilder send is for emitting data to uibuildier
                
                uibuilder.send({//this is where the msg is being sent
                //when you click this sends the room number
                    'topic': "data from click on room",
                    'payload': {
                        'message': selectedRoomID
                    }
                })
              
                */

              //console.log(node.selected);
              node.selected = true;
              node.material = clickedTexture;
              let userData = JSON.stringify(node.userData);
              //console.log(typeof(String(userData)), String(userData));
            }
            else {
  
              //if current node was selected and deselecting then make the selectedUUID null
              
              node.selected = false;
              console.log(node.uuid);//consider storing uuid in a variable with a scope above loader call, to check for double selection
              node.material = availableTexture;
              selectedUUID = null;
              console.log(selectedUUID);
  
            }
          
          })

        }//if not disabled logic end

                

        
        //console.log(node);
        //console.log(node.geometry);

        /* edgeHelper = new THREE.EdgesGeometry(node.geometry);
        edgeLine = new THREE.LineSegments(edgeHelper, new THREE.LineBasicMaterial({color: 0x222222, linewidth: 6}));
        //edgeLine.position.set(5,5,5)
        return scene.add( edgeLine ); */
        //edgeLine.position.set(node.position);
        

        //console.log("found mesh");
      }
        
    })
    //console.log(toIntersect);
		//floorplan.material.color.setHex(0x234567);
    //floorplan.material = THREE.TextureLoader("/resources/model/_150010BX.jpg");
    //floorplan.position.set(0,0,0);
    
    //floorplan.rotateX(Math.PI/4);
    //floorplan.updateMatrixWorld();
    //floorplan.rotateY(Math.PI/2);
    //floorplan.rotation.set(0,0,0);
    //floorplan.scale.set(0.25,0.25,0.25);


    scene.add(floorplan);   

    
    //this is a bounding box for the parsed floorplan
    boxCenter = new THREE.Box3().setFromObject(floorplan);
    
    //function centerObjects() {//functions work, they can be unfunctioned now
      //center the floorplan in the display canvas


      //console.log(boxCenter);
    boxCenter.getCenter(floorplan.position);
      //console.log(boxCenter.center(floorplan.position));
    floorplan.localToWorld(boxCenter);
    floorplan.position.multiplyScalar(-1);
      
    //}		

    //centerObjects();

    //zoom to fit the bounding box of the floorplan
    //console.log(boxCenter.max.x, boxCenter.max.y, boxCenter.min.x, boxCenter.min.y);

    //function cameraZoomFix() {
    camera.zoom = Math.min((aspectRatio*viewSize)/(boxCenter.max.x - boxCenter.min.x),
    viewSize/(boxCenter.max.y - boxCenter.min.y)) * 0.4;//issue with zoom
      console.log(Math.min((aspectRatio*viewSize)/(boxCenter.max.x - boxCenter.min.x),
      viewSize/(boxCenter.max.y - boxCenter.min.y)) * 0.4)
    camera.updateProjectionMatrix();
    camera.updateMatrix();
    //}
    //cameraZoomFix();
    //camera.lookAt(floorplan);


    //displays the bounding box
    boxHelper = new THREE.BoxHelper( floorplan, 0xffff00 );
    scene.add( boxHelper );

    //this is a little thing that renders the black helper lines
    floorplan.traverse(function(thing){
      if (thing.isMesh && !thing.disabled) {
        var edgeLine,edgeHelper;
        edgeLine = new THREE.BoxHelper( thing, 0x333333 );
        return scene.add( edgeLine );
      }
      else if(thing.name="Text"){
        //try to render the text here
        //define FontLoader and use that to load font
        //look at textgeometry
        //console.log(thing);
        //thing.material = symbolTexture;
        

      }
      
    })//end of floorplan traverse and edgeline logic

	}, undefined, function(err) {
		console.log(err);
	});//loader load logic end

  oControls = new THREE.OrbitControls(camera, renderer.domElement);
  


  oControls.enabled = false;
  //oControls.target = cube.getWorldPosition();//use bounding box of floorplan
  oControls.enableRotate = true;
  oControls.enableZoom = true;
  oControls.zoomSpeed = 1;
  oControls.enablePan = true;
  

/* 
  light = new THREE.PointLight(0xFFFFFF, 1, 100);
  light.position.set(-10, 10, 0);
  light.castShadow = true; */
  //scene.add(light);
/* 
  directionalLight = new THREE.DirectionalLight(0xFFFFFF);
  directionalLight.position.set(-40, 150, -10);
 */
  //scene.add(directionalLight);

  spotlight = new THREE.SpotLight(0xFFFFFF, 1, 1300);

  spotlight.position.set(0, 30, 0);
  scene.add(spotlight);


  camera.position.z = 0;//you could change perspective by changing the camera position settings
  camera.position.y = 50;
  camera.position.x = 0;
  //let vector = {
    //x : 20,
    //y : 25,
    //z : 32
  //};
  //console.log(cube.position);
  //camera.lookAt([0,0,0]);
  function resizeCanvasToDisplay() {
    //console.log("resized");
    const canvas = renderer.domElement;
    //read canvas size
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;

    //adjust display size to match
    if(canvas.width !== width || canvas.height !== height) {
      renderer.setSize(width, height, false);
      camera.aspect = width / height;
      camera.updateProjectionMatrix;
    }
  }

  function render() {
  //functionize and redo zoom and centering here
    resizeCanvasToDisplay()
    requestAnimationFrame(render);
    oControls.update();
    renderer.render(scene, camera);
  
  }
  requestAnimationFrame(render);

}

})();


    
//})