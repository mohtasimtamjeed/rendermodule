Readme
=======

### files
 *draw.js*  
 this is the main file which takes in data and renders the gltf file

 *server.js*
 used to run dev server

 index.js require.js config.js
 were files required to run the module with uibuilder


### Structure of the data that the render file takes

floor = {
  id: 1,
  file: "01_1OG_Text_Mesh.gltf",
  //"02_2OG_Text_Mesh.gltf" for floor2
  //"03_3OG_Text_Mesh.gltf" for floor3
  //"04_4OG_Text_Mesh.gltf" for floor4
  //"05_5OG_Text_Mesh.gltf" for floor5
  //"06_6OG_Text_Mesh.gltf" for floor6
  selected: true,
  rooms: {
      0: {"floorid": 1,
          "id": 111,
          "roomNumber": 111,
          "available": true
      },
      1: {"floorid": 1,
          "id": 122,
          "roomNumber": 122,
          "available": true
      },
      3: {"floorid": 1,
          "id": 130,
          "roomNumber": 130,
          "available": true
      }
      
  }
}


### Events available in the render file for room objects:

  * click(line 333)
  * mouseover(line 322)
  * mouseout(line 326)
  * touchstart(line 268)
  * touchend(line 299)
  * uibuilder.send: this emits message through uibuilder node for example id or roomnumber of clicked room(draw.js line 363)
  * uibuilder.onChange: this receives a change in message object transmitted to uibuilder node (line 85)


### JS Libraries and plugins required:

1. Three js **three.min.js**
2. GLTFLoader **GLTFloader.js**
3. OrbitControls **OrbitControls.js**
4. Threex.domevents **threex.domevents.js**

Files are provided in the repo to avoid version conflicts

the GLTFLoader, OrbitControls and threex.domevents plugins all have THREE js as a dependency 
this means they require the THREE object defined in three.min.js to function  produces errors otherwise


### gltf files to render
the api gives the name of the file to load with the corresponding floor  
but the gltf files are also required to load
the files are included in the repo as well
the name of the file to be rendered has to be included with the floor object the module receives


the gltf loader loads the file async(lines 182, 205)